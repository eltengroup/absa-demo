## Getting Started 

### Requirements:

- [x] Setup a local Kubernetes cluster using Kind (https://kind.sigs.k8s.io/)
- [x] Write an admission webhook server that will add a label of your choosing to all pods created (https://kubernetes.io/blog/2019/11/26/develop-a-kubernetes-controller-in-java/ is a good start)
- [x] Add the necessary mutating admission webhook configuration to your Kubernetes cluster to get it working
- [x] Record a screencast of your admission webhook in action

### Some assumptions
- You have a configured ```kubectl```
- You have a healthy ```docker``` environment 


#### Lets start by creating a cluster

The command will do the following:
- Source environment variable from ```env.sh```. This is where you can custonmize a few things
- Create the cluster itself
- Set the namespace
- Update kubeconfig to this namespace
- Label the cluster with ```policy-enabled=enabled```
- Create self signed certs using ```certs.sh```

> For simplicity, this need to be executed from this current directory for simplicity

```
./absa.sh cluster create
```

- Verify you have secrets created matching your variables

    ```$ kubectl get secrets```

    ```$ ls -lsa certs``` Should contains generated certs

- Verify that key ```caBundle``` was substituted in ```deployments/webhooks.yaml```


From this point onwards we will be using this deploy script a lot. The script is not perfect but it does the job.

Deploy supporting apps:

These apps should be deployed first in the order listed.

- webhooks-server-py
- webhooks (configurations)

```
./absa.sh apps deploy webhook
```

Then deploy the webhook configuration
```
./absa.sh config webhooks enable
```

There are two nginx yaml files under ```deployments/``` which can be used for testing. One has labels, and the other has no labels.

```
kubectl apply -f deployments/sample-1.yaml
kubectl apply -f deployments/sample-2.yaml
kubectl apply -f deployments/sample-3.yaml
``` 

Check deployment labels

```shell script
kubectl get deploy --show-labels -w
```