### User Guide

This assumes you have pipenv installed. If not head over to [pypi](https://pypi.org/project/pipenv/) for more

```shell script
pipenv --python 3.7
pipenv install
pipenv shell
```
Exit the python shell environment
```shell script
pipenv --rm
exit
```

> Side note: This methods also assumes a healthy k8s cluster with ```kubectl``` command line tool which is not covered in here

### Running locally

env variables the may be passed to the app depending on where you run this service.

- CERT_PATH : provides path to your certificates generated using ```certs.sh``` in the parent project

Once in the python environment of you choosing from the above step, execute:
```shell script
export CERT_PATH=certs 
python app.py
``` 

### Running inside a kubernetes cluster (local assumed)

Create these environment variables using a config map:

```shell script
kubectl create secret generic webhook-server-certs \
        --from-file=server-cert.key="$CERT_PATH/server-cert.key" \
        --from-file=server-cert.pem="$CERT_PATH/server-cert.pem" \
        --dry-run -o yaml |
    kubectl apply -f -
```

Deploy the service:
```shell script
kubectl apply -f manifests/deployment.yaml
```

### Deploy using scripts

If you are lazy like me, you can run the corresponding scripts for deploying either local or to your cluster (```builder.sh```)
You are now ready to receive admission reviews from kubernetes :)