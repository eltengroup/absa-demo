import logging
from pprint import pformat

logging.basicConfig(format='[%(levelname)s]: %(message)s', level=logging.DEBUG)
log = logging.getLogger(__name__)


def logger_h(content):
    log.debug("-" * 100)
    if isinstance(content, (list,)):
        for line in content:
            log.debug("[ %s ]" % line)
    else:
        log.debug("[ %s ]" % content)
    log.debug("-" * 100)


def logger_debug(content):
    log.debug("*" * 100)
    if isinstance(content, (list,)):
        for line in content:
            log.debug("# %s #" % line)
    else:
        log.debug("# %s #" % content)
    log.debug("*" * 100)


def logger(content):
    log.debug("%s " % pformat(content))