#!flask/bin/python

import base64
import copy
import os
from pprint import pprint

import jsonpatch as jsonpatch
from flask import Flask, jsonify, request

from functions import logger_h, logger

app = Flask(__name__)

labels = {
    'general': {
        'cost-center': 'general-cost-centre'
    },
    'marketing': {
        'cost-center': 'MK10043'
    },
    'development': {
        'cost-center': 'DV10074',
        'project': 'kubernetes'
    }
}


def add_labels(clone_object, group_key):
    try:
        group_labels = labels.get('general') if group_key is None else labels.get(group_key)
        if 'labels' in clone_object['metadata']:
            clone_object['metadata']['labels'].update(group_labels)
        else:
            clone_object['metadata']['labels'] = group_labels
    except KeyError as ke:
        print('Error occurred while reading deployment object keys', ke)


def read_group_annotation():
    if 'annotations' not in request.json['request']['object']['metadata']:
        print('annotations not present as all')
        return None
    if 'group' not in request.json['request']['object']['metadata']['annotations']:
        print('group key not present in annotations')
        return None
    return request.json['request']['object']['metadata']['annotations']['group']


def validate_request(key):
    valid = True
    try:
        pprint(request.json['request']['object']['metadata'])
        if key is None:
            return False

        if key in labels.keys():
            group_labels = labels.get(key)
            for label in group_labels:
                if label not in request.json['request']['object']['metadata']['labels']:
                    return False
        else:
            valid = False

    except KeyError as ke:
        print('Error occurred while reading deployment object keys', ke)
    return valid


def build_admission_review(spec, group_key):

    clone_object = copy.deepcopy(spec)

    add_labels(clone_object, group_key)

    patch = jsonpatch.JsonPatch.from_diff(spec, clone_object)
    logger(str(patch))
    admission_review = {
        'apiVersion': 'admission.k8s.io/v1',
        'kind': 'AdmissionReview',
        'response': {
            'allowed': True,
            'uid': request.json['request']['uid'],
            'patch': base64.b64encode(str(patch).encode('utf-8')).decode(),
            'patchType': 'JSONPatch',
            'status': {
                'code': 200,
                'message': 'Patch applied successfully'
            }
        }
    }
    logger(admission_review)
    return admission_review


@app.route('/mutate', methods=['POST'])
def mutate():
    """
    Web hook invoked from kubernetes API. A few things are assumed:
    1. Request objects metadata should contain a group annotation to
       help determine the labels to add for that abject. The labels
       could be read from some store based on this annotation.
    2. Each object belongs to a department / team and the annotation
       added as a mandatory field perhaps in the CI/CD pipeline
    """
    object_key = request.json['request']['object']

    group_annotation = read_group_annotation()
    response = build_admission_review(object_key, None) \
        if group_annotation is None \
        else build_admission_review(object_key, group_annotation)

    logger_h('Admin review response')

    return response


@app.route('/health', methods=['GET'])
def health():
    status = {'status': 'UP'}
    return jsonify(status)


if __name__ == '__main__':
    cert_path = os.getenv('CERT_PATH', '/etc/certs')
    app.run(ssl_context=(
        '%s/server-cert.pem' % cert_path,
        '%s/server-cert.key' % cert_path,
    ),
        debug=True, host='0.0.0.0', port=443
    )
