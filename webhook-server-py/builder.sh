#!/bin/bash

function usage() {
  echo "Usage : $0"
  printf "%s\t\t%s\n" "Building:" "[--build|-b]"
  printf "%s\t\t%s\n" "Run:"      "[--run|-r]"
  printf "%s\t\t%s\n" "Pushing:"  "[--push|-p]"
  printf "%s\t\t%s\n" "Deleting:" "[--delete|-d]"
  exit
}

source ../env.sh

version=$APP_VERSION
cluster=$CLUSTER_NAME
ns=$CLUSTER_NAMESPACE

if [ -z "$cluster" ]; then cluster="demo-cluster"; fi
if [ -z "$ns" ]; then ns="demo-namespace"; fi

if [ $# -lt 1 ]; then
  usage
fi

image=egroup/webhook-server-py
image_tag=
if [[ -z "$version" ]]; then
  image_tag=egroup/webhook-server-py
else
  image_tag=egroup/webhook-server-py:"$version"  
fi
container=webhook-server-py

case "$1" in
-b | --build)
  echo "√ Creating container $image ..."
  docker image build -t "$image" .
  docker tag "$image" "$image"
  ;;
-r | --run)
  echo "√ Creating container $image ..."
  docker image build -t "$image" .
  docker run -d --rm -it --name "$container" \
   -e CERT_PATH="$CERT_PATH" \
   -e SOCKET_HOST="$SOCKET_HOST" \
   -e SOCKET_PORT="$SOCKET_PORT" \
   -p 443:443 \
   "$image"
   docker ps -a | grep "$container"
  ;;
-p | --push)
  echo "√ Pushing container into cluster $image ..."
  if [[ "$(docker images -q "$image" 2>/dev/null)" == "" ]]; then
    docker image build -t "$image_tag" .
  fi
  if [[ "$REGISTRY" ]] && [[ "$REGISTRY" == 'remote' ]]; then
    echo "# Pushing image to docket hub"
    docker login
    docker push "$image_tag"
  else
    echo "# Adding image to kind cluster"
    kind load docker-image "$image_tag" --name eltengroup
  fi
  ;;
-d | --delete)
  echo "√ Deleting container $image ..."
  docker stop "$container" 2>/dev/null
  # docker ps -q -f name="$image"
  if [[ "$(docker images -q "$image" 2>/dev/null)" != "" ]]; then
    docker images | grep "$container"
    docker container prune -f
    docker system prune -f --volumes
  fi
  ;;
*)
  echo "Incorrect option supplied!"
  usage
  ;;
esac
