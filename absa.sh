#!/usr/bin/env bash

set -o errexit

source env.sh

function usage() {
  echo '-----------------------------------------------------------------------------'
  echo "Usage : $0"
  echo '-----------------------------------------------------------------------------'
  printf "%s\t%s\n" "Creating a cluster:" "cluster create"
  printf "%s\t%s\n" "Deleting a cluster:" "cluster delete"
  echo '-----------------------------------------------------------------------------'
  printf "%s\t%s\n" "Enable  configs:" "config enable [ mutation | validation ]"
  printf "%s\t%s\n" "Disable configs:" "config disable [ mutation | validation ]"
  echo '-----------------------------------------------------------------------------'
  printf "%s\t\t%s\n" "Deploying apps:" "apps build [ webhook ]"
  printf "%s\t\t%s\n" "Deploying apps:" "apps deploy [ webhook ]"
  printf "%s\t\t%s\n" "Deploying apps:" "apps delete [ webhook ]"
  echo '-----------------------------------------------------------------------------'
  exit
}

function cleanUp() {
  rm -rf certs/
  mkdir certs/
  docker rmi "$(docker images -f "dangling=true" -q)" 2>/dev/null
}

if [ $# -lt 1 ]
then
    usage
fi

cluster=$CLUSTER_NAME
ns=$CLUSTER_NAMESPACE

if [ -z "$cluster" ]; then cluster="demo-cluster"; fi
if [ -z "$ns" ]; then ns="sample-namespace"; fi

case "$1" in
cleanup)
    cleanUp
    ;;

cluster)

    case "$2" in
    create)
      kind create cluster --name "$cluster"
      kubectl create ns "$ns"
      kubectl config set-context --current --namespace="$ns"
      kubectl label ns "$ns" policy-labels=enabled
      sleep 2
      bash certs.sh
        ;;
    delete)
      kind delete cluster --name "$cluster"
      cleanUp
        ;;
    esac
    ;;

config)

    case "$2" in
    enable)
        case "$3" in
        mutation)
            echo '√ Enabling mutation config'
            kubectl apply -f deployments/webhooks.yaml
            ;;

        validation)
            echo '√ Enabling validation config'
            kubectl apply -f deployments/webhooks.yaml
            ;;

        esac
        ;;

    disable)
        case "$3" in
        mutation)
            echo '√ Disabling mutation config'
            kubectl delete -f deployments/webhooks.yaml
            ;;

        validation)
            echo '√ Disabling validation config'
            kubectl delete -f deployments/webhooks.yaml
            ;;

        esac
        ;;
    esac
    ;;

apps)

    case "$2" in
    build)
        case "$3" in
        webhook)
            echo '√ Building webhook server'
            cd webhook-server-py || exit
            bash builder.sh -b
            bash builder.sh -p
            export WH_SERVER_NAME="$APP_NAME"
            export WH_NS="$ns"
            cat manifests/_deployment_.yaml | envsubst > manifests/deployment.yaml
            cd ..
            ;;
        esac
        ;;

    deploy)
        case "$3" in
        webhook)
            echo '√ Deploying webhook server'
            kubectl apply -f webhook-server-py/manifests/deployment.yaml
            ;;
        esac
        ;;

    delete)
        case "$3" in
        webhook)
            echo '√ Deleting webhook server'
            kubectl delete -f webhook-server-py/manifests/deployment.yaml
            ;;
        esac
        ;;

    logs)
        case "$3" in
        webhook)
            echo '√ Getting logs webhook server'
            kubectl logs -f --selector=app=webhook-server
            ;;
        esac
        ;;
    esac
    ;;
esac
