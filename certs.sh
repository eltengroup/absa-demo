#!/usr/bin/env bash

set -o errexit

source env.sh

app=$APP_NAME
namespace=$CLUSTER_NAMESPACE
csr_name=$CLUSTER_CSR_NAME

if [ -z "$APP_NAME" ]; then app="demo-app"; fi
if [ -z "$CLUSTER_NAMESPACE" ]; then namespace="demo-namespace"; fi
if [ -z "$CLUSTER_CSR_NAME" ]; then csr_name="${app}.${namespace}.svc"; fi

echo "... creating ${app}.key"
openssl genrsa -out "$CERT_PATH/${app}.key" 2048

echo "... creating $CERT_PATH/${app}.csr"
cat >"$CERT_PATH"/csr.conf<<EOF
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${app}
DNS.2 = ${app}.${namespace}
DNS.3 = ${csr_name}
DNS.4 = ${csr_name}.cluster.local
EOF
echo "openssl req -new -key $CERT_PATH/${app}.key -subj \"/CN=${csr_name}\" -out $CERT_PATH/${app}.csr -config $CERT_PATH/csr.conf"
openssl req -new -key "$CERT_PATH/${app}.key" -subj "/CN=${csr_name}" -out "$CERT_PATH/${app}.csr" -config "$CERT_PATH/csr.conf"

echo "... deleting existing csr, if any"
echo "kubectl delete csr ${csr_name} || :"
kubectl delete csr ${csr_name} || :
	
echo "... creating kubernetes CSR object"
echo "kubectl create -f -"
kubectl create -f - <<EOF
apiVersion: certificates.k8s.io/v1beta1
kind: CertificateSigningRequest
metadata:
  name: ${csr_name}
spec:
  groups:
  - system:authenticated
  request: $(cat $CERT_PATH/${app}.csr | base64 | tr -d '\n')
  usages:
  - digital signature
  - key encipherment
  - server auth
EOF

SECONDS=0
while true; do
  echo "... waiting for csr to be present in kubernetes"
  echo "kubectl get csr ${csr_name}"
  kubectl get csr ${csr_name} > /dev/null 2>&1
  if [ "$?" -eq 0 ]; then
      break
  fi
  if [[ $SECONDS -ge 60 ]]; then
    echo "[!] timed out waiting for csr"
    exit 1
  fi
  sleep 2
done

kubectl certificate approve ${csr_name}

SECONDS=0
while true; do
  echo "... waiting for serverCert to be present in kubernetes"
  echo "kubectl get csr ${csr_name} -o jsonpath='{.status.certificate}'"
  serverCert=$(kubectl get csr ${csr_name} -o jsonpath='{.status.certificate}')
  if [[ $serverCert != "" ]]; then 
    break
  fi
  if [[ $SECONDS -ge 60 ]]; then
    echo "[!] timed out waiting for serverCert"
    exit 1
  fi
  sleep 2
done

echo "... creating ${app}.pem cert file"
echo "\$serverCert | openssl base64 -d -A -out $CERT_PATH/${app}.pem"
echo "${serverCert}" | openssl base64 -d -A -out "$CERT_PATH/${app}.pem"

cp "$CERT_PATH/${app}.pem" webhook-server-py/certs/server-cert.pem
cp "$CERT_PATH/${app}.key" webhook-server-py/certs/server-cert.key

kubectl create secret generic $app-certs \
        --from-file=server-cert.key="$CERT_PATH/${app}.key" \
        --from-file=server-cert.pem="$CERT_PATH/${app}.pem" \
        --dry-run -o yaml |
    kubectl apply -f -

export CA_BUNDLE=$(kubectl get configmap -n kube-system extension-apiserver-authentication -o=jsonpath='{.data.client-ca-file}' | base64 | tr -d '\n')
export WH_SERVER_NAME="$APP_NAME"
export WH_NAME="webhook-config"
export WH_NS="$namespace"

cat deployments/tpl/_webhooks_.yaml | envsubst > deployments/webhooks.yaml
